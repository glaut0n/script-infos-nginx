#!/usr/bin/env bash
#
# listarInfos.sh - Este script irá retornar algumas informações sobre o arquivo de configuração do NGINX.
#                  Para que o script funcione, é necessário que o arquivo de configurações (default.conf)
#                  esteja no mesmo diretório que o script (listarInfos.sh)
#         
# Site:       https://gitlab.com/glaut0n
# Autor:      Glaucia Lauton
# Manutenção: Glaucia Lauton
#
# ------------------------------------------------------------------------ #
#  Exemplos:
#      $ ./listarInfos.sh -p -r
#      Neste exemplo, foram passados dois parâmetros para o shell: -p e -r
#      Esses parâmetros irão exibir qual a porta está sendo utilizada pelo NGINX
#      e também quais são as configurações de proxy reverso.
#
# ------------------------------------------------------------------------ #
# ------------------------------------------------------------------------ #
#
# ------------------------------- VARIÁVEIS ------------------------------ #
PORTAS=$(cat default.conf | grep listen | grep -v '#')
PROXY_PASS=$(cat default.conf | grep proxy_pass | grep -v '#')
DIR_ROOT=$(cat default.conf | grep root | grep -v '#')
INDICE=$(cat default.conf | grep index | grep -v '#')
ACCESS_LOG=$(cat default.conf | grep access_log)
VERSAO_SCRIPT="v1.1"
MENSAGEM_AJUDA="
    $0 - [opções]

    -h - Menu de Ajuda 
    -v - Versão do script
    -p - Exibe a porta de utilização do Nginx.
    -r - Exibe as configurações de proxy reverso do Nginx.
    -d - Exibe qual o diretório raíz de arquivos do Nginx.
    -i - Exibe qual é o arquivo de indice do Nginx.
    -a - Exibe se o access log está habilitado.
"
#Variáveis que servirão como chhaves de valor para a utilização da funcionalidade "shift"
#que está no case.
CHAVE_PORTA=0
CHAVE_PROXY=0
CHAVE_DIR=0
CHAVE_INDICE=0
CHAVE_ACCESS=0
# ------------------------------------------------------------------------ #
# ------------------------------------------------------------------------ #

while test -n "$1"; do
#É feito um teste para verificar se algum parâmetro de fato está sendo passado
#para a variável $1. Caso não esteja, nem entrará no case.
    case "$1" in

        -h) echo "$MENSAGEM_AJUDA" && exit 0                                             ;;
        -v) echo "$VERSAO_SCRIPT" && exit 0                                              ;;
        -p) CHAVE_PORTA=1                                                                ;;
        -r) CHAVE_PROXY=1                                                                ;;
        -d) CHAVE_DIR=1                                                                  ;;
        -i) CHAVE_INDICE=1                                                               ;;
        -a) CHAVE_ACCESS=1                                                               ;;
        *) echo "Opção Inválida. Valide as opções corretas com o -h"
    esac
    shift
    #O shift irá verificar os parâmetros que serão passados e remaneja-los para a variável $1.
done 

# ------------------------------------------------------------------------ #
#Essa estrutura verifica se as chaves tem o valor 1. Se tiverem, realizarão as ações determinadas.
[ $CHAVE_PORTA -eq 1 ] && PORTAS=$(echo -e "Porta Utilizada:\n" "$PORTAS") && echo $PORTAS
[ $CHAVE_PROXY -eq 1 ] && PROXY_PASS=$(echo -e "Proxy Pass:" $PROXY_PASS) && echo $PROXY_PASS
[ $CHAVE_DIR -eq 1 ]   && DIR_ROOT=$(echo "Diretório Raíz: " $DIR_ROOT) && echo $DIR_ROOT
[ $CHAVE_INDICE -eq 1 ] && INDICE=$(echo "Arquivo de Índice: " $INDICE) && echo $INDICE
[ $CHAVE_ACCESS -eq 1 ] && ACCESS_LOG=$(echo "String de Access Log: " $ACCESS_LOG) && echo $ACCESS_LOG

